package pdf;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;

public class Main {
  private static final String carpeta = "C:\\Users\\0014870\\Documents\\docu java\\";
  public static void main(String[] args) throws IOException {

    crearCarpetas(arrayFicheros());

  }

  public static void crearImagenes(String nombre) throws IOException {
    PDDocument document = PDDocument.load(new File(carpeta + nombre + ".pdf"));
    PDFRenderer pdfRenderer = new PDFRenderer(document);
    for (int page = 0; page < document.getNumberOfPages(); ++page) {
      BufferedImage bim = pdfRenderer.renderImageWithDPI(page, 300, ImageType.RGB);
      ImageIOUtil.writeImage(bim, String.format(carpeta + nombre + "\\" + "%d.%s", page + 1, "jpg"), 300);
    }
    document.close();
  }

  public static File[] arrayFicheros() {
    return new File(carpeta).listFiles();
  }

  public static void crearCarpetas(File[] files) throws IOException {
    for (File file : files) {
      File f = new File(carpeta + file.getName().replace(".pdf", ""));
      f.mkdirs();
      crearImagenes(f.getName());
    }
  }
}
